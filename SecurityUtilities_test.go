package utilities

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewSecurity(t *testing.T) {

	tests := []struct {
		name   string
		logger *zerolog.Logger
	}{
		{
			"Asserts new security created where logger passed",
			NewLogger(),
		},
		{
			"Asserts new security create where no logger passed",
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewSecurity(nil); got != nil {
				assert.NotNil(t, got)
			}
		})
	}
}

func Test_security_ApplySha256(t *testing.T) {
	s := NewSecurity(nil)
	tests := []struct {
		name string
		data string
	}{
		{
			"Asserts value can be hashed",
			"testInput",
		},
		{
			"Asserts value cannot be hashed with 0 string",
			"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := s.ApplySha256(tt.data); err != nil {
				assert.Error(t, err)
			} else {
				assert.NotEqual(t, tt.data, got)
			}
		})
	}
}

func Test_security_Applysha256Bytes(t *testing.T) {
	s := NewSecurity(nil)
	tests := []struct {
		name string
		data []byte
	}{
		{
			"Asserts value can be hashed",
			[]byte("testInput"),
		},
		{
			"Asserts value cannot be hashed with 0 string",
			[]byte(""),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := s.ApplySha256Bytes(tt.data); err != nil {
				assert.Error(t, err)
			} else {
				assert.NotEqual(t, tt.data, got)
			}
		})
	}
}

func Test_security_CreatePassword(t *testing.T) {
	s := NewSecurity(nil)
	tests := []struct {
		name     string
		password string
	}{
		{
			"Asserts input value is accepted (len 8)",
			"test1234",
		},
		{
			"Asserts input value is not accepted (len 7)",
			"test123",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := s.CreatePassword(tt.password); err != nil {
				assert.Error(t, err)
			} else {
				assert.NotEqual(t, tt.password, got)
			}
		})
	}
}

func Test_security_GenerateAccessToken(t *testing.T) {
	s := NewSecurity(nil)

	t.Run("Validate two keys are not the same", func(t *testing.T) {
		secretKey := "someSecretKey"
		first, err1 := s.GenerateAccessToken(secretKey, "test@test.com", "admin")
		fmt.Println(first)

		second, err2 := s.GenerateAccessToken(secretKey, "tester@test.com", "user")
		fmt.Println(second)

		assert.NotEqual(t, first, second)
		assert.NoError(t, err1)
		assert.NoError(t, err2)
	})
}

func Test_security_ValidateAccessToken(t *testing.T) {

	secretKey := "someSecretKey"
	t.Run("Validate an expired token - fail", func(t *testing.T) {
		token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3Jpc2VkIjp0cnVlLCJlbWFpbCI6InRlc3RlckB0ZXN0LmNvbSIsImV4cCI6MTY0MzU3ODk1MCwiaWF0IjoxNjQzNTc4OTUwLCJyb2xlIjoidXNlciJ9.m3RPXkIdLDGZsuKp3ddLGNNt-knu0wM7rzuA35rWobw"

		s := NewSecurity(nil)
		err := s.ValidateAccessToken(secretKey, token)
		assert.Error(t, err)
	})
	t.Run("Validate an invalid token - fail", func(t *testing.T) {
		token := "fail the test"
		s := NewSecurity(nil)
		err := s.ValidateAccessToken(secretKey, token)
		assert.Error(t, err)
	})
	t.Run("Validate a token - Fail, invalid key", func(t *testing.T) {
		s := NewSecurity(nil)
		token, _ := s.GenerateAccessToken(secretKey, "test@test.com", "user")

		wrongKey := "moreSecretKeyz"
		err := s.ValidateAccessToken(wrongKey, token)
		assert.Error(t, err)
	})
	t.Run("Validate a token - true", func(t *testing.T) {
		s := NewSecurity(nil)
		token, _ := s.GenerateAccessToken(secretKey, "test@test.com", "user")

		err := s.ValidateAccessToken(secretKey, token)
		assert.NoError(t, err)
	})

}

func Test_security_ValidatePassword(t *testing.T) {
	s := NewSecurity(nil)

	type args struct {
		hashed string
		input  string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Assert that input and hashed match",
			args{
				hashed: "$2a$13$w.V2eUWfOpm4hc0.a0J41eD9NKKTJj/scNgT6u.C12UOzRtthoUxy",
				input:  "Test1234",
			},
			true,
		},
		{
			"Assert that input and hashed DO NOT match",
			args{
				hashed: "$2a$13$w.V2eUWfOpm4hc0.a0J41eD9NKKTJj/scNgT6u.C12UOzRtthoUxy",
				input:  "Geronimo",
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := s.ValidatePassword(tt.args.hashed, tt.args.input); got != tt.want {
				t.Errorf("ValidatePassword() = %v, want %v", got, tt.want)
			}
		})
	}
}

