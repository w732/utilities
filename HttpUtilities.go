package utilities

import (
	"github.com/rs/zerolog"
	"io"
	"net/http"
)

type httpUtilities struct {
	logger *zerolog.Logger
	client http.Client
}

func NewHttpUtilities(logger *zerolog.Logger) HttpUtilities {
	if logger == nil {
		logger = NewLogger()
	}

	return &httpUtilities{
		logger: logger,
		client: http.Client{},
	}
}

type HttpUtilities interface {
	Request(method, url string, headers map[string]string, body io.Reader) (*http.Response, error)
}

func (h *httpUtilities) Request(method, url string, headers map[string]string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		h.logger.Error().Msg(err.Error())
		return nil, err
	}

	for key, value := range headers {
		req.Header.Add(key, value)
	}

	res, err := h.client.Do(req)
	if err != nil {
		h.logger.Error().Msg(err.Error())
		return nil, err
	}

	return res, nil
}
