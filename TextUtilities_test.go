package utilities

import (
	"testing"
)

func Test_textUtilities_ContainsFold(t1 *testing.T) {
	type args struct {
		s     string
		chars string
	}
	tests := []struct {
		name   string
		args   args
		want   bool
	}{
		{
			"All caps vs lower",
			args{
				s:     "hello, world",
				chars: "HELLO, WORLD",
			},
			true,
		},
		{
			"all caps partial",
			args{
				s:     "hello, world",
				chars: "O, WO",
			},
			true,
		},
		{
			"all caps ",
			args{
				s:     "HELLO, world",
				chars: "ello, WO",
			},
			true,
		},
		{
			"camel case",
			args{
				s:     "hello, world",
				chars: "hElLo, WoRlD",
			},
			true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := NewTextUtilities(nil)
			if got := t.ContainsFold(tt.args.s, tt.args.chars); got != tt.want {
				t1.Errorf("ContainsFold() = %v, want %v", got, tt.want)
			}
		})
	}
}
