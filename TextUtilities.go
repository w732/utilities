package utilities

import (
	"github.com/rs/zerolog"
	"strings"
)

type textUtilities struct {
	logger *zerolog.Logger
}

func NewTextUtilities(logger *zerolog.Logger) TextUtilities {
	if logger == nil {
		logger = NewLogger()
	}

	return &textUtilities{logger: logger}
}

type TextUtilities interface {
	ContainsFold(s, chars string) bool
}

func (t *textUtilities) ContainsFold(s, chars string) bool {
	lowerS := strings.ToLower(s)
	lowerChars := strings.ToLower(chars)

	return strings.ContainsAny(lowerS, lowerChars)
}
