package utilities

import (
	"context"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

type client struct {
	Database mongo.Database
	Logger *zerolog.Logger
}

// NewMongoClient creates a new mongo.Client as a reusable object.
func NewMongoClient(mongoUri string, logger *zerolog.Logger) (Client, error) {
	if logger == nil {
		logger = NewLogger()
	}
	clientOptions := options.Client().ApplyURI(mongoUri)

	if mClient, err := mongo.NewClient(clientOptions); err != nil {
		logger.Error().Msg(err.Error())
		return nil, err
	} else {
		db := mClient.Database(os.Getenv("DATABASE_NAME"))
		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		if err = db.Client().Connect(ctx); err != nil {
			logger.Error().Msg(err.Error())
			return nil, err
		} else {
			return &client{
				Database: *db,
				Logger: logger,
			}, nil
		}
	}
}

type Client interface {
	NewRecord(record interface{}, collection string) error
	GetRecord(record interface{}, filter bson.D, collection string) error
}

func (c *client) NewRecord(record interface{}, collection string) error {
	_, err := c.Database.Collection(collection).InsertOne(context.Background(), record)
	if err != nil {
		c.Logger.Error().Msg(err.Error())
		return err
	}
	return nil
}

func (c *client) GetRecord(record interface{}, filter bson.D, collection string) error {
	if err := c.Database.Collection(collection).FindOne(context.TODO(), filter).Decode(record); err != nil {
		c.Logger.Error().Msg(err.Error())
		return err
	}
	return nil
}