package utilities

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/rs/zerolog"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type security struct {
	MinimumCost int
	logger *zerolog.Logger
}

func NewSecurity(logger *zerolog.Logger) Security {
	if logger == nil {
		logger = NewLogger()
	}

	return &security{
		MinimumCost: 10,
		logger:      logger,
	}
}

type Security interface {
	GenerateAccessToken(secretKey, email, role string) (string, error)
	ValidateAccessToken(secretKey, tokenValue string) error
	ApplySha256Bytes(data []byte) (string, error)
	ApplySha256(data string) (string, error)
	CreatePassword(password string) (string, error)
	ValidatePassword(hashed, input string) bool
}

func (s *security) GenerateAccessToken(secretKey, email, role string) (string, error) {
	signingKey := []byte(secretKey)
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	initTime := time.Now()
	claims["authorised"] = true
	claims["email"] = email
	claims["role"] = role
	claims["iat"] = initTime.Unix()
	claims["exp"] = initTime.Add(time.Minute * 90).Unix()

	tokenValue, err := token.SignedString(signingKey)
	if err != nil {
		s.logger.Error().Msgf("something went wrong! %s", err.Error())
		return "", err
	}

	return tokenValue, nil
}

func (s *security) ValidateAccessToken(secretKey, tokenValue string) error {
	var mySigningKey = []byte(secretKey)

	token, err := jwt.Parse(tokenValue, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("there was an error in parsing")
		}
		return mySigningKey, nil
	})
	if err != nil {
		s.logger.Error().Msg(err.Error())
		return err
	}

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return nil
	} else {
		return errors.New("not authorised")
	}
}

// ApplySha256Bytes creates a hash of byte array
func (s *security) ApplySha256Bytes(data []byte) (string, error) {
	if len(data) <= 0 {
		return "", errors.New("data cannot be nil")
	}
	sum256 := sha256.Sum256(data)
	return fmt.Sprintf("%x", sum256), nil
}

// ApplySha256 creates a hash of string data
func (s *security) ApplySha256(data string) (string, error) {
	return s.ApplySha256Bytes([]byte(data))
}

// CreatePassword generates a password with the use of bcrypt lib
func (s *security) CreatePassword(password string) (string, error) {
	if len(password) >= 8 {
		hashed, err := bcrypt.GenerateFromPassword([]byte(password), s.MinimumCost)
		if err != nil {
			fmt.Println(err.Error())
			return "", err
		}
		return string(hashed), nil
	}
	return "", errors.New("password cannot be less than 8 chars")
}

// ValidatePassword validates the input against hashed
func (s *security) ValidatePassword(hashed, input string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(input)); err != nil {
		return false
	}
	return true
}